export class Article {
    id;
    username;
    title;
    content;
    categorie;

    constructor (username, title, content, categorie, id = null) {
        this.username = username;
        this.title = title;
        this.content = content;
        this.categorie = categorie;
        this.id = id;
    }
}