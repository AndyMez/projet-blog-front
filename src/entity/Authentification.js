export class Authentification {
    id;
    username;
    mail;
    password;

    constructor (username, mail, password, id = null) {
        this.username = username;
        this.mail = mail;
        this.password = password;
        this.id = id;
    }
}
